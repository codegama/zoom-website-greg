<!DOCTYPE html>
<html lang="en">

<head>

    <title>{{Setting::get('site_name')}}</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ Setting::get('site_icon') }}" />

    <!-- Custom CSS -->
    <link href="{{asset('admin-assets/css/style.css')}}" rel="stylesheet">

    <!-- Toast CSS -->
    <link href="{{asset('admin-assets/plugins/toaster/toastr.min.css')}}" rel="stylesheet" />


    @include('layouts.admin.scripts')

    <?php echo Setting::get('header_scripts'); ?>
   
</head>

<body>

    @yield('content')


    <?php echo Setting::get('google_analytics'); ?>

    <?php echo Setting::get('body_scripts'); ?>
    
</body>

</html>