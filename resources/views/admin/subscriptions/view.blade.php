@extends('layouts.admin')

@section('page_header',tr('subscriptions'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_subscriptions')}}</li>
@endsection

@section('content')

<div class="card">

	<div class="card-header bg-info"> 

		<h4 class="m-b-0 text-white">{{tr('view_subscriptions')}}</h4>

	</div>

    <div class="card-body">

    	<div class="row">

            <div class="col-6">

                <!-- Card content -->
                <div class="card-body">

                    <div class="template-demo">

                        <table class="table mb-0">

                          <thead>
                           
                          </thead>

                          <tbody>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('title') }}</b></td>
                                <td class="pr-0 text-right"><div>{{$subscription_details->title}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('no_of_months') }}</b></td>
                                <td class="pr-0 text-right"><div>{{formatted_plan($subscription_details->plan,$subscription_details->plan_type)}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('amount') }}</b></td>
                                <td class="pr-0 text-right"><div>{{formatted_amount($subscription_details->amount)}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('no_of_hrs') }}</b></td>
                                <td class="pr-0 text-right"><div>{{$subscription_details->no_of_hrs_formatted}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('no_of_users') }}</b></td>
                                <td class="pr-0 text-right"><div>{{$subscription_details->no_of_users_formatted}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('subscribers') }}</b></td>
                                 <td class="pr-0 text-right">
                                    <a href="{{ route('admin.subscriptions.users' , ['subscription_id' => $subscription_details->id] ) }}">{{ $total_subscribers }}</a>
                                </td>
                            </tr>

                            <tr>

                              <td class="pl-0"> <b>{{ tr('status') }}</b></td>

                              <td class="pr-0 text-right">

                                    @if($subscription_details->status == USER_PENDING)

                                        <span class="card-text badge badge-danger badge-md text-uppercase">{{tr('pending')}}</span>

                                    @elseif($subscription_details->status == USER_APPROVED)

                                        <span class="card-text  badge badge-success badge-md text-uppercase">{{tr('approved')}}</span>

                                    @else

                                        <span class="card-text label label-rouded label-menu label-danger">{{tr('declined')}}</span>

                                    @endif

                              </td>

                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('created_at') }}</b></td>
                                <td class="pr-0 text-right"><div>{{ common_date($subscription_details->created_at, Auth::guard('admin')->user(),'d M Y H:i:s') }}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('updated_at') }}</b></td>
                                <td class="pr-0 text-right"><div>{{ common_date($subscription_details->updated_at,Auth::guard('admin')->user(),'d M Y H:i:s') }}</div></td>
                            </tr>

                          </tbody>

                        </table>

                    </div>

                </div>

            </div>

    		<div class="col-6">

                <div class="row">

                    @if($subscription_details->description)

                        <h5 class="col-md-12">{{tr('description')}}</h5>
                        <p class="col-md-12 text-muted text-word-wrap">{{$subscription_details->description}}</p>

                    @endif
                    
                </div>
                <hr>
				<div class="row">

                    <div class="col-6">

                        @if(Setting::get('is_demo_control_enabled') == YES)

                            <a href="javascript:;" class="btn btn-primary btn-block">{{tr('edit')}}</a>

                            <a href="javascript:;" class="btn btn-danger btn-block">{{tr('delete')}}</a>

                        @else

                            <a class="btn btn-primary btn-block" href="{{ route('admin.subscriptions.edit', ['subscription_id' => $subscription_details->id])}}">{{tr('edit')}}</a>

                            <a class="btn btn-danger btn-block" href="{{route('admin.subscriptions.delete', ['subscription_id' => $subscription_details->id])}}" onclick="return confirm(&quot;{{tr('subscription_delete_confirmation' , $subscription_details->title)}}&quot;);">{{tr('delete')}}</a>

                        @endif

                    </div>

                    <div class="col-6">

                    	@if($subscription_details->status == USER_APPROVED)

                            <a class="btn btn-danger btn-block" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}" onclick="return confirm(&quot;{{$subscription_details->title}} - {{tr('subscription_decline_confirmation')}}&quot;);" >
                                {{ tr('decline') }} 
                            </a>

                        @else
                            
                            <a class="btn btn-success btn-block" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}">
                                {{ tr('approve') }} 
                            </a>
                               
                        @endif

                    </div> 

                </div>

    		</div>

            

        </div>

    </div>

</div>

@endsection

