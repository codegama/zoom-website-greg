<div class="card-body">

    @if(Setting::get('is_demo_control_enabled') == NO )

        <form  method="POST" action="{{route('admin.subscriptions.save')}}" enctype="multipart/form-data" role="form">

    @else

        <form class="forms-sample" role="form">

    @endif

        @csrf

        <div class="form-body">

            <h3 class="card-title">{{tr('subscriptions')}}</h3>

            <hr>

            <input type="hidden" name="subscription_id" value="{{ $subscription_details->id }}">

            <div class="row p-t-20">

                <div class="form-group col-md-6">

                    <label class="control-label">{{ tr('title') }}<span class="admin-required">*</span></label>
                    <input type="text" required name="title" class="form-control" id="title" value="{{$subscription_details->title ?? old('title') }}" placeholder="{{ tr('title') }}" pattern = "[a-zA-Z,0-9\s\-\.]{2,100}">
                     
                </div>

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('description')}}<span class="admin-required">*</span></label>
                    <textarea class="form-control" name="description" required placeholder="{{ tr('description') }}">{{$subscription_details->description ?? old('description') }}</textarea>
                    
                </div>

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('no_of_months')}}<span class="admin-required">*</span></label>
                    <input type="number" min="1" max="12" pattern="[0-9][0-2]{2}"  required name="plan" class="form-control" id="plan" value="{{ $subscription_details->plan ?? old('plan') }}" title="{{ tr('please_enter_plan_month') }}" placeholder="{{ tr('no_of_months') }}">
                    
                </div>

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('no_of_users')}}<span class="admin-required">*</span></label>
                    <input type="number" required value="{{$subscription_details->no_of_users ?? old('no_of_users') }}" name="no_of_users" class="form-control" id="no_of_users" placeholder="{{ tr('no_of_users') }}" step="any">
                    
                </div>

                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('no_of_hrs')}}<span class="admin-required">*</span></label>
                    <input type="number" required value="{{$subscription_details->no_of_hrs ?? old('no_of_hrs') }}" name="no_of_hrs" class="form-control" id="no_of_hrs" placeholder="{{ tr('no_of_hrs') }}" step="any">
                    
                </div>


                <div class="form-group col-md-6">

                    <label class="control-label">{{tr('amount')}}<span class="admin-required">*</span></label>
                    <input type="number" required value="{{$subscription_details->amount ??  old('amount')}}" name="amount" class="form-control" id="amount" placeholder="{{ tr('amount') }}" step="any">
                    
                </div>


            </div>

        </div>

        <div class="form-actions">

            @if(Setting::get('is_demo_control_enabled') == NO )

                <button type="submit" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>

            @else

                <button type="button" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>
                
            @endif

            <button type="reset" class="btn reset-btn btn-pill">{{tr('reset')}}</button>

        </div>

    </form>

</div>